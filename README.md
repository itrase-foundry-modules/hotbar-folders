# Hotbar Folders
Add-On Module for Foundry VTT

Adapted from Monk's Hotbar Expansion. This is an update to the standard Hotbar to vertically expand a slot on the hotbar into a group of items.

## Installation
Simply use the install module screen within the FoundryVTT setup. It is extremely unlikely that this will work with any other add-on that interacts with the hotbar.

## Usage & Current Features
Hovering over a slot in the hotbar will display every item in that slot for each of the five hotbar rows. Adding new items to the hotbar still requires you to add them to the base hotbar.

## Known Issues
This is an alpha release, I'm sure there are a lot.

## License
This Foundry VTT module, written by itrase, is licensed under [GNU GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html), supplemented by [Commons Clause](https://commonsclause.com/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
