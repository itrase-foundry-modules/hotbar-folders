export let i18n = key => {
    return game.i18n.localize(key);
};

const WithHotbarFolders = (Hotbar) => {
    class HotbarFolders extends Hotbar {
        constructor(...args) {
            super(...args);

            this.macrolist = [];
            this._pagecollapsed = [];
			for (let i = 0; i < 10; i++) {
				this._pagecollapsed.push(true);
			}
			this.timeOutId = 0;
			this.macroRange = Array.from(Array(10).keys());
        }

        static get defaultOptions() {
            return mergeObject(super.defaultOptions, {
                id: "hotbar",
                template: "./modules/hotbar-folders/templates/hotbar.html",
                popOut: false,
                dragDrop: [{ dragSelector: ".macro-icon", dropSelector: ".macro-list" }]
            });
        }

        async getData() {
            const data = await super.getData();

            const numberOfRows = 5;
            this.macrolist = [];

            for (let i = 1; i <= numberOfRows; i++) {
                let macros = this._getMacrosByPage(i);
                this.macrolist.push({ page: i, macros: macros, selected: i == this.page });
            }

            data.showArrows = true;
			data.macroRange = this.macroRange;
            data.macrolist = this.macrolist;
			// TODO: We don't use this variable as far as I can tell, but things break without this line.
            data.pageClass = [(this._pagecollapsed ? 'collapsed' : '')].filter(c => c).join(' ');

            return data;
        }

        activateListeners(html) {
            super.activateListeners(html);
            html.mouseenter(this._onHoverHotbar.bind(this));
            html.mouseleave(this._onLeaveHotbar.bind(this));
			for (let i = 0; i < 10; i++) {
				name = "#macro-list [data-slot='" + (i + 1) + "']";
				html.find(name).hover(this._onEnterHotbar(i).bind(this));
			}
			html.find('.macro-active').click(this._onClickMacroAlso.bind(this));
        }

        async _onLeaveHotbar(event) {
            event.preventDefault();
			var timeoutId = setTimeout(() => {
				return this.collapseAllPagesExcept(10);
			}, 300)
			this.timeOutId = timeoutId;
        }
		
		async _onClickMacroAlso(event) {
			console.log('wow');
            event.preventDefault();
			return this.collapseAllPagesExcept(10);
		}

        async _onHoverHotbar(event) {
            event.preventDefault();
			clearTimeout(this.timeOutId);
        }
			
		_onEnterHotbar(num) {
			return async function(event) {
				event.preventDefault();
				return this.collapseAllPagesExcept(num);
			}
        }		
		
		async collapseAllPagesExcept(num) {
			for (let i = 0; i < 10; i++) {
				if ((i != num) && (!this._pagecollapsed[i])) {
					var temp = this.collapsePage(i);
				} else if ((i == num) && (this._pagecollapsed[i])) {
					var temp = this.expandPage(i);
				}
			}
			return temp;
		}

		collapsePage(num) {
			const pageName = "#hotbar-page-" + num;
			var page = this.element.find(pageName);
			return new Promise(resolve => {
				page.slideUp(200, () => {
					page.addClass("collapsed");
					this._pagecollapsed[num] = true;
					resolve(true);
				});
			});
		}

        expandPage(num) {
			const pageName = "#hotbar-page-" + num;
            var page = this.element.find(pageName);
			for (let i = 0; i < 5; i++) {
				if (this.macrolist[i].macros[num].cssClass == "inactive" || i == 0) {
					$(page.children()[i]).addClass("collapsed");
				}
			}
            return new Promise(resolve => {
                page.slideDown(200, () => {
                    page.removeClass("collapsed");
                    this._pagecollapsed[num] = false;
                    resolve(true);
                });
            });
        }

    }

    const constructorName = "HotbarFolders";
    Object.defineProperty(HotbarFolders.prototype.constructor, "name", { value: constructorName });
    return HotbarFolders;
}

Hooks.on('init', () => {
    CONFIG.ui.hotbar = WithHotbarFolders(CONFIG.ui.hotbar);
});